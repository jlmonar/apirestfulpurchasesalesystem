<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Product;
use App\Category;
use App\Transaction;
use Illuminate\Support\Facades\DB;

/**
 * Los seeders son clases que permiten usar los factories, o propios seeds creados por
 * nosotros, para insertar información normalmente en la base de datos.
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Para evitar problema de inconsistencias con las claves foráneas
        //asi que desactivamos temporalmente la verificacion de claves foráneas
        //Ejecutamos sentencia SQL
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        /* Truncate sirve para borrar todos los registros de una tabla */
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();

        //Esto hace que durante la ejecucion de los seeders se desactiven los listeners,
        //de este modo evitariamos que se ejecuten eventos como el envio de correos para
        //usuarios nuevos cuando se ejecuta el seeder
        User::flushEventListeners();
        Category::flushEventListeners();
        Product::flushEventListeners();
        Transaction::flushEventListeners();

        $cantidadUsuarios = 200;
        $cantidadCategorias = 30;
        $cantidadProductos = 1000;
        $cantidadTransacciones = 1000;

        factory(User::class, $cantidadUsuarios)->create();
        factory(Category::class, $cantidadCategorias)->create();

        factory(Product::class, $cantidadProductos)->create()->each(
            function ($producto) {
                //Se obtiene array de categorias aleatoria, el tamaño del array varia
                //entre 1 a 5 elementos. Pluck es utilizado para obtener únicamente el
                //campo 'id' en el arreglo.
                $categorias = Category::all()->random(mt_rand(1,5))->pluck('id');

                //Attach permite insertar el registro en la tabla intermedia que une
                //a las tablas categories y products.
                $producto->categories()->attach($categorias);
            }
        );

        factory(Transaction::class, $cantidadTransacciones)->create();
    }
}
