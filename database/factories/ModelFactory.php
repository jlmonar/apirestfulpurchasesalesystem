<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\User;
use App\Category;
use App\Product;
use App\Transaction;
use App\Seller;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/**
 * Los factories son funciones que nos permiten crear instancias de diferentes modelos o
 * diferentes recursos por medio de datos falsos. Los factories hacen uso de una dependencia
 * llamada faker que es la que permitirá la creación de datos falsos.
 */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'verified' => $verificado = $faker->randomElement([User::USUARIO_VERIFICADO, User::USUARIO_NO_VERIFICADO]),
        'verification_token' => $verificado == User::USUARIO_VERIFICADO ? null : User::generarVerificationToken(),
        'admin' => $faker->randomElement([User::USUARIO_ADMINISTRADOR, User::USUARIO_REGULAR]),
    ];
});

$factory->define(Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
    ];
});


$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph(1),
        'quantity' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement([Product::PRODUCTO_DISPONIBLE, Product::PRODUCTO_NO_DISPONIBLE]),
        'image' => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
        //'seller_id' => User::inRandomOrder()->first()->id,
        'seller_id' => User::all()->random()->id,
    ];
});

$factory->define(Transaction::class, function (Faker\Generator $faker) {
    //Se obtienen todos los usuarios que tengan al menos un producto (significaria que es un vendedor)
    //y se elige uno aleatorio.
    $vendedor = Seller::has('products')->get()->random();
    //Puede ser un usuario que no necesariamente ya haya comprado algo, pero está a punto de hacerlo
    //El comprador tampoco puede ser el mismo vendedor del producto, por eso usamos except().
    $comprador = User::all()->except($vendedor->id)->random();
    return [
        'quantity' => $faker->numberBetween(1, 3),
        //Un comprador no puede comprar sus propios productos.
        'buyer_id' => $comprador->id,
        //Tiene que ser el id de un producto vendido por un usuario diferente al comprador
        'product_id' => $vendedor->products->random()->id
    ];
});