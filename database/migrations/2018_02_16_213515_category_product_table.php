<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Estamos lidiando con una tabla pivote que resulta de la relación muchos a muchos entre
 * 'products' y 'categories'.
 * Al momento de crear esta migración, en la consola debemos especificar el nombre de las
 * 2 tablas implicadas en orden alfabético, en singular (no usamos el nombre de las tablas,
 * sino de los modelos) y separados por guión bajo '_', por lo tanto debe ser con el nombre:
 * category_product.
 */
class CategoryProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_product', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_product');
    }
}
