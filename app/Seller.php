<?php

namespace App;

use App\Scopes\SellerScope;
use App\Transformers\SellerTransformer;

class Seller extends User
{
    public $transformer = SellerTransformer::Class;

    protected static function boot() {
        parent::boot();

        static::addGlobalScope(new SellerScope());
    }

    /**
     * Un vendedor TIENE muchos productos.
     */
    public function products() {
        return $this->hasMany(Product::class);
    }
}
