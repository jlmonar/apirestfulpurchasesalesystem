<?php

namespace App\Traits;

/*
 * Usamos un trait porque nos da la posibilidad de implementarlo opcionalmente, es decir, si usamos
 * el trait es porque las acciones para un Policy específico se van a permitir para el administrador,
 * pero si no lo usamos es porque simplemente no se van a ejecutar.
 */
trait AdminAction
{
    public function before($user, $ability) {
        /*
         * Esta función se va a ejecutar before, es decir antes, este va a recibir el usuario
         * actualmente autenticado y la habilidad, es decir, si puede ver si puede crear
         * si puede vender, etc.
         * Según lo que retorne esta función, se va a permitir al usuario realizar esa acción.
         * Si before es true, la acción se va a permitir inmediatamente sin ir a ejecutar las demás.
         * Si before retorna false entonces la acción se va a denegar sin ir a ejecutar ninguna
         * de las otras acciones, es decir sin ir a verificar ni view, create, etc.
         * En nuestro caso queremops permitir el acceso únicamente si el usuario es administrador.
         */
        if ($user->esAdministrador()) {
            return true;
        }
        /*
         * Si retornamos falso, entonces no se va a comprobar ninguna de las otras condiciones y
         * no se va a permitir la acción, y si el usuario no es administrador, pues de todas formas
         * es posible que sea el usuario propietario y cualquiera de las otras condiciones le
         * de acceso.
         */
    }
}