<?php


namespace App\Scopes;
/**
 * Created by PhpStorm.
 * User: José Luis
 * Date: 21/03/2018
 * Time: 11:49
 */

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Scope;

/*
 * Un Scope es una consulta que podemos ejecutar de manera global en un modelo cada vez que se realicen consultas sobre
 * el mismo.
 */
class BuyerScope implements Scope {

    /*
     * Esta es la función que se ejecutará para aplicar el scope como tal.
     * Recibe como parametros el Builder el cual es el constructor de la consulta, y también recibe el modelo como tal.
     */
    public function apply(Builder $builder, Model $model) {
        /*
         * Lo que hará esta función apply es modificar la consulta típica del modelo y agregar has('transactions').
         */
        $builder->has('transactions');
    }
}