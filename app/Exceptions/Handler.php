<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Asm89\Stack\CorsService;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Esta clase permite manejar las diferentes excepciones que se puedan presentar.
 */
class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $response = $this->handleExcepcion($request, $exception);
        /*
         * Le pedimos a Laravel a través de un helper llamado 'app' que resuelva el servicio de CORS
         * Una vez hecho esto, llamamos al método addActualRequestHeaders que recibe dos parámetros, uno es
         * la respuesta sobre la cual vamos a agregar esas cabeceras, y la otra es la petición, esto
         * actua por referencia, lo cual quiere decir que no necesitamos asignar esto a la respuesta,
         * ya la respuesta es automáticamente modificada y la podemos devolver.
         * Pero el problema es saber cual es la respuesta, la respuesta es lo que retorna cada una
         * de las posibles excepciones, pero seria tedioso cambiar todos los returns para asignarlo
         * a un response,a parte que nos traería algunos problemas, entonces, la solución sencilla
         * es mover toda la parte del código donde retornamos los errores de alguna excepcion, a un
         * nuevo método.
         */
        app(CorsService::class)->addActualRequestHeaders($response, $request);

        return $response;

    }

    public function handleExcepcion($request, Exception $exception) {
        /*
        * Es importante manejar directamente las excepciones de tipo ValidationException, esto es necesario
        * ya que actualmente, las excepciones de este tipo estan siendo manejadas por el método parent::render
        * Sin embargo, no podemos depender únicamente del método render  para manejar diferente tipos
        * de excepciones que puedan surgir, puesto que el método render tiende a mostrar gran detalle SOLO de algunas
        * excepciones, lo cual no es adecuado si ya estamos en etapa de producción.
        */
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }

        /*
         * Manejo excepciones que se dan cuando no se encuantra una instancia de un modelo especifico.
         */
        if ($exception instanceof ModelNotFoundException) {
            //Obtengo el nombre del modelo que no se pudo encontrar.
            $model = strtolower(class_basename($exception->getModel()));
            return $this->errorResponse("No existe ninguna instancia de {$model} con el id especificado", 404);
        }

        /*
         * Manejo excepciones de AUTENTICACIÓN, las cuales se dan cuando se quiere acceder a un recurso SIN estar autenticado.
         */
        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
        }

        /*
         * Manejo excepciones de AUTORIZACIÓN, las cuales se dan cuando se quiere acceder a un recurso (sin importar si se está autenticado)
         * sin que el suario tenga AUTORIZACIÓN para acceder a dicho recurso.
         */
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse('No posee permisos para ejecutar esta acción', 403);
        }

        /*
         * Manejo excepciones cuando el usuario quiere acceder a rutas que no existen (errores de tipo 404).
         */
        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse('No se encontró la url especificada', 404);
        }

        /*
         * Manejo excepciones que se dan cuando especificamos erroneamente el tipo del método en una petición.
         */
        if ($exception instanceof  MethodNotAllowedHttpException) {
            return $this->errorResponse('El método especificado en la petición no es válido.', 405);
        }

        /*
         * Manejo direntes tipo de excepciones de tipo HttpException ya que son muchos este tipo de excepciones.
         */
        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(), $exception->getCode());
        }

        /*
         * Manejo errores que se dan cuando se ejecuta una sentencia en la base de datos, por ejemplo, cuando se quiere
         * eliminar un recurso que esta relacionado con tro.
         */
        if ($exception instanceof QueryException) {
            //Obtengo el código del tipo de error en el query.
            $codigo = $exception->errorInfo[1];

            if ($codigo == 1451) {//Se quiere eliminar un recurso que esta relacionado con algún otro.
                return $this->errorResponse('No se puede eliminar de forma permanente el recurso porque está relacionado con algún otro', 409);
            }
        }

        /*
         * Este tipo de excepción proviene de la aplicación web (no de la api), por lo tanto ya no vamos
         * a retornar un json, sino una respuesta html, lo mejor es hacer una redirección a la ubicación
         * original del usuario.
         */
        if ($exception instanceof TokenMismatchException) {
            //Redirecciono al sitio anterior, adicionalmente, con los valores originales
            //con with() estamos enviando cada una de las entradas que el usuario introdujo
            //al intentar iniciar sesión y se las vamos a poner otra vez.
            return  redirect()->back()->withInput($request->input());
        }

        /*
         * Si estamos en modo de depuración, entonce vamos a dejar que el método render haga su trabajo,
         * esto para conocer a detalle el problema en desarrollo.
         */
        if (config('app.debug')) {
            return parent::render($request, $exception);
        }

        /*
         * Luego de todas las condiciones que tenemos, si ninguna de las posibles excepciones para las cuales estamos preparados coincide,
         * entonces vamos a suponer que es una falla inesperada (error 500).
         */
        return $this->errorResponse('Falla inesperada. Intente luego.', 500);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($this->isFrontend($request)) {
            return redirect()->guest('login');
        }

        return $this->errorResponse('No autenticado', 401);
    }

    /**
     * Independientemente si el clioente requiere json o no, se va a retornar siempre una respuesta de
     * tipo json (en caso que ocurra un error o excepción.)
     *
     * @param ValidationException $e
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = $e->validator->errors()->getMessages();

        if ($this->isFrontend($request)) {
            /*
             * Si la petición es una petición ajax, entonces retornamos una repsuesta de tipo json
             * indicando unicamente los errores y el codigo 422. Si no es ajax, vamos a retornar
             * una dirección con los valores de entrada indicando los errores.
             */
            return $request->ajax() ? response()->json($errors, 422) : redirect()
                ->back()
                ->withInput($request->input())
                ->withErrors($errors);
        }

        return $this->errorResponse($errors, 422);
    }

    /**
     * Determina si la aplicación está recibiendo una petición desde nuestro sitio web.
     *
     * @param $request
     * @return bool
     */
    public function isFrontend($request)
    {
        /*
         * Se deben cumplir ambas condiciones para determinar si la petición proviene del sitio web, que
         * la petición acepte html y que al menos uno de los middlewares en la ruta de la petición
         * es el middleware 'web'.
         */
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }
}
