<?php

namespace App;
use App\Scopes\BuyerScope;
use App\Transaction;
use App\Transformers\BuyerTransformer;

class Buyer extends User
{
    public $transformer = BuyerTransformer::Class;

    /*
     * Este método es utilizado para construir e inicializar el modelo, y en este caso lo vamos a utilizar para
     * especificar el scope que el modelo va a utilizar.
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope(new BuyerScope());
    }

    /**
     * Un comprador TIENE muchas transacciones.
     */
    public function transactions() {
        return $this->hasMany(Transaction::class);
    }
}
