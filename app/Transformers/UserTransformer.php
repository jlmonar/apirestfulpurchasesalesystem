<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier' => (int)$user->id,
            'name' => (string)$user->name,
            'email' => (string)$user->email,
            'isVerified' => (int)$user->verified,
            'isAdministrator' => ($user->admin === 'true'),
            'creationDate' => (string)$user->created_at,
            'updateDate' => (string)$user->updated_at,
            'deletedDate' => isset($user->deleted_at) ? (string)$user->deleted_at : null,

            //Aplico HATEOAS para facilitar la navegabilidad entre recursos.
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('users.show', $user->id),
                ],
            ],
        ];
    }

    public static function originalAttribute($index) {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'isVerified' => 'verified',
            'isAdministrator' => 'admin',
            'creationDate' => 'created_at',
            'updateDate' => 'updated_at',
            'deletedDate' => 'deleted_at',
            'password' => 'password',
            'password_confirmation' => 'password_confirmation',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index) {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'email' => 'email',
            'verified' => 'isVerified',
            'admin' => 'isAdministrator',
            'created_at' => 'creationDate',
            'updated_at' => 'updateDate',
            'deleted_at' => 'deletedDate',
            'password' => 'password',
            'password_confirmation' => 'password_confirmation',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
