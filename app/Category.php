<?php

namespace App;

use App\Transformers\CategoryTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    public $transformer = CategoryTransformer::Class;

    protected  $dates = ['deleted_at'];

    /**
     * Los atributos fillable son aquellos  atributos que pueden ser asignados de manera
     * masiva. Una asignación se hace de manera masiva en laravel cuando se realiza el
     * establecimiento de tal atributo por medio del método create o update.
     * Modelo::create([
     * 'name' => 'algo',
     * 'description' => 'algo2'
     * ]);
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * Este atributo hidden lo que hace es ocultar los atributos incluidos allí al momento
     * de convertir la representación de este modelo en un array.
     * PD: Cualquier atributo dentro de hidden, será ocultado en las respuestas del api restful
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * La categoria PERTENECE a muchos productos (Aqui estamos lidiando con una relación muchos a muchos
     * entre categories y products)
     */
    public function products() {
        return $this->belongsToMany(Product::Class);
    }
}
