<?php

namespace App;

use App\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    /*
     * HasApiTokens:
     * El trait HasApiTokens nos permite acceder al token por medio del atributo $accessToken,
     * y tiene una serie de relaciones, podemos acceder a los clientes del usuario, los tokens,
     * el token particular que se está usando en ese momento especifico de la petición, también
     * se puede especificar que se puede hacer con ese token a partir de un scope, también hay un
     * método que nos permite crear un nuevo token para este usuario y establecerlo y establecerlo
     * como una especie de sesión activa.
     */
    use Notifiable, SoftDeletes, HasApiTokens;

    const USUARIO_VERIFICADO = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR = 'false';

    public $transformer = UserTransformer::Class;    

    /*
     * Al especificar a que tabla corresponde, entonces le estamos diciendo a laravel que este modelo
     * corresponde a la tabla users, y los modelos de Seller y Buyer al extender de User, cambiaran
     * su comportamiento al momento de ejecutar los seeders, ya que Laravel ya sabe que los modelos
     * que extienden de User corresponden a la tabla users. Si no se hiciera esto, al momento de ejecutar
     * los seeders Laravel va a suponer que los mdelos Seller y Buyer tendran tablas llamadas sellers y
     * buyers respectivamente, y no es asi, ya que solo existe una tabla users donde se encuentran los
     * buyers y sellers.
     */
    protected $table = 'users';

    /*
     * Aqui indico que este campo deberá ser tratado como una fecha.
     */
    protected  $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /* PD: Cualquier atributo dentro de hidden, será ocultado
     * en las respuestas del api restful
     */
    protected $hidden = [
        'password',
        'remember_token',
        //'verification_token',
    ];

    /*
     * Mutador: Es una función publica usada para transformar un atributo que queremos registrar en la base de datos.
     */
    public function setNameAttribute($valor) {
        //strtolower: con esto hacemos que el nombre esté con minúsculas.
        $this->attributes['name'] = strtolower($valor);
    }

    /*
     * Accesor: Es una función pública usada para transformar un atributo que queremos obtener.
     */
    public function getNameAttribute($valor) {
        //return ucfirst($valor); //ucfirst: Esto convierte en mayuscula solo la primer letra de la primer palabra.
        return ucwords($valor); //ucwords: Conviernte en mayúscula la primer letra de cada palabra.
    }

    public function setEmailAttribute($valor) {
        $this->attributes['email'] = strtolower($valor);
    }

    public function esVerificado()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador()
    {
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    /**
     * Permite obtener token de verificacion generado automáticamente. Se lo crea como
     * un método estático ya que no requerimos directamente de una instancia de User
     * para generar el token.
     */
    public static function generarVerificationToken() {
        return str_random(40);
    }
}
