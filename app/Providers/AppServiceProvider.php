<?php

namespace App\Providers;

use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 * Los providers son clases que podemos utilizar en algún momento para realizar
 * el registro de algún tipo de dependencia o funcionalidad que requerimos sea
 * ejecutada de forma automatizada durante el comienzo de una petición.
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Hace que todas las cadenas de caracteres que usemos en las migraciones
         * y base de datos tendrán un tamaño por defecto de 191 y no de 256 como se
         * hacia usualmente
         * */
        Schema::defaultStringLength(191);

        User::created(function ($user) {
            retry(5, function() use ($user) {
                Mail::to($user->email)->send(new UserCreated($user));
            }, 100);
        });

        User::updated(function($user) {
            if ($user->isDirty('email')) {
                retry(5, function() use ($user) {
                    Mail::to($user->email)->send(new UserMailChanged($user));
                }, 100);
            }
        });

        //Este evento se ejecutará cada vez que un producto es actualizadO, y
        //cambiara el estado a NO DISPONIBLE cuando la cantidad del producto llegue a 0
        Product::updated(function($product) {
           if ($product->quantity == 0 && $product->estaDisponible()) {
               $product->status = Product::PRODUCTO_NO_DISPONIBLE;
               $product->save();
           }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
