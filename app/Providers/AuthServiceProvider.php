<?php

namespace App\Providers;

use App\Buyer;
use App\Policies\BuyerPolicy;
use App\Policies\ProductPolicy;
use App\Policies\SellerPolicy;
use App\Policies\TransactionPolicy;
use App\Policies\UserPolicy;
use App\Product;
use App\Seller;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Buyer::class => BuyerPolicy::class,
        Seller::class => SellerPolicy::class,
        User::class => UserPolicy::class,
        Transaction::class => TransactionPolicy::class,
        Product::class => ProductPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /*
         * Registramos nuestro propio gate que es quien va a permitir el acceso de ciertas rutas
         * SOLO a administradores.
         */
        Gate::define('admin-action', function ($user) {
            /*
             * Dependiendo si aqui se retorna verdadero o falso, la acción se permitirá únicamente
             * a través del gate.
             */
            return $user->esAdministrador();
        });

        /*
         * Registramos todas las rutas requeridas para el correcto funcionamiento de Laravel Passport,
         * para ello, usamos el siguiente método el cual ya es previamente definido, este nos permite
         * registrar el listado completo de rutas para utilizar.
         */
        //Passport::routes();

        //Lo iniciamos así porque estamos usando el prefix 'api' para las rutas de la api.
        Passport::routes(null, ['prefix' => 'api/oauth']);

        /*
         * Definimos el tiempo de expiración de los tokens creados por passport. (Para esto, generamos
         * una fecha a partir del momento exacto en que se está generando el token y una cantidad
         * requerida adicional de segundos, minutos, horas, etc.)
         */
        Passport::tokensExpireIn(Carbon::now()->addMinutes(30));

        /*
         * Definimos el tiempo de expiración para los refresh tokens, el cual durará durante mucho
         * mas tiempo. Se podrá utilizar un refresh token para refrescar un token a mas tardar 30
         * dias despues de haber sido generado por primera vez. Esto quiere decir, que cuando el token
         * original expira, el cliente tiene máximo hasta 30 dias para utliizar un refresh token y
         * obtener uno nuevo, pues después de los 30 dias tendría que utilizar el flujo de autorizacion
         * para obtener un nuevo token por parte del usuario.
         */
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));

        /*
         * Habilitamos el Grant Type implicito, ya que no viene por defecto en la API.  Este Grant Type me
         * permite que, en lugar de obtener un código de autorización, obtenemos inmediatamente un
         * acces token, (asi evitamos tener que pedir primero el codigo de autorización).
         * Se utiliza este tipo de Grant Type cuando el cliente no tiene la posibilidad para almacenar
         * sus credenciales, lo que se refiere especialmente al secret. Normalmente esto se da en
         * dispositivos móviles o simplemente basados en el cliente de manera que no pueden almacenar
         * de una manera segura sus credenciales para acceder al sistema.
         */
        Passport::enableImplicitGrant();

        /*
         * Registramos los scopes posibles para definir los alcances o los permisos que pueden
         * llegar a tener en un momemnto específico un  cliente cualquiera.
         */
        Passport::tokensCan([
            /*
             * Scope que permite hacer compras al cliente en nombre del usuario.
             */
            'purchase-product' => 'Crear transacciones para comprar productos determinados',
            /*
             * Scope que permite operar sobre los diferentes productos que pueden pertenecer al
             * usuario, ya sea crearlos, eliminarlos o actualizarlos o simplemente visualizarlos.
             */
            'manage-products' => 'Crear, ver, actualizar y eliminar productos',
            /*
             * Scope que permite operar sobre la cuenta del usuario de diferentes formas.
             */
            'manage-account' => 'Obtener la información de la cuenta, nombre, email, estado (sin contraseña),
                                 modificar datos como email, nombre y contraseña. No puede eliminar la cuenta',
            /*
             * Scope que permite leer información como categorias, productos comprados, ventas, etc.
             */
            'read-general' => 'Obtener información geeral, categorias donde se compra y se vende, productos
                                vendidos o comprados, transacciones, compras y ventas',
        ]);
    }
}
