<?php

namespace App;

use App\Transformers\TransactionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    public $transformer = TransactionTransformer::Class;

    protected  $dates = ['deleted_at'];

    protected  $fillable = [
        'quantity',
        'buyer_id',
        'product_id',
    ];

    /**
     * Una transacción PERTENECE a un comprador (transaccion es quien tiene la clave foránea 'buyer_id').
     */
    public function buyer() {
        return $this->belongsTo(Buyer::class);
    }

    /**
     * Una transacción PERTENECE a un producto (transaccion es quien tiene la clave foránea 'product_id').
     */
    public function product() {
        return $this->belongsTo(Product::class);
    }
}
