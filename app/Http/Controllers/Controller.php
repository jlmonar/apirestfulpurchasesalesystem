<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * Los controllers son las clases donde podemos llevar a cabo toda la interacción con
 * la lógica de nuestra aplicación, es decir, interactuar con los modelos para obtener
 * información y luego llevarla a las vistas para mostrar tal información.
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
