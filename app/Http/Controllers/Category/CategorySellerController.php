<?php

namespace App\Http\Controllers\Category;

use App\Category;
use App\Http\Controllers\ApiController;

class CategorySellerController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Category $category
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $this->allowedAdminAction();

        $sellers = $category->products()
            ->with('seller')
            ->get()
            ->pluck('seller')
            ->unique()
            ->values();

        return $this->showAll($sellers);
    }
}
