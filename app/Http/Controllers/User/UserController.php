<?php

namespace App\Http\Controllers\User;

use App\Mail\UserCreated;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Support\Facades\Mail;

class UserController extends ApiController
{
    public function __construct()
    {
        /*
         * Se aplica el middleware para todos los métodos, excepto para store, resend y verify.
         * La ruta verify es la única que será de acceso publica (sin necesidad de estar protegida) esto
         * es debido a que esta ruta es llamada directamente cuando los usuarios hacen click en el enlace
         * para verificar su correo electrónico, y este no tiene manera de enviarnos un access token.
         */
        $this->middleware('auth:api')->except(['store', 'resend', 'verify']);
        /*
         * Permitimos que un cliente valido de nuestro sistema esté en capacidad de registrar un
         * usuario y de reenviar el correo de verificacion.
         */
        $this->middleware('client.credentials')->only(['store', 'resend']);

        $this->middleware('transform.input:' . UserTransformer::class)->only(['store', 'update']);

        /*
         * El scope 'manage-account' permite a un cliente cualquiera, gestionar las cuentas del usuario,
         * siempre y cuando tenga garantizado este scope, pero se tiene ciertas restricciones, por ejemplo,
         * no vamos a permitirle visualizar la lista completa de usuarios ymucho menos remover cuentas
         * de usuario (Estas dos acciones requieren de permisio administrativos). La creacion de usuarios
         * (store) será de acceso público, es decir, cualquiera en cierto modo puede crear usuarios siempre
         * y cuando sea un cliente valido de nuestro sistema. La destrucción y visualizacion de todos los
         * usuarios será permitido únicamente para usuarios administradores.
         */
        $this->middleware('scope:manage-account')->only(['show','update']);

        $this->middleware('can:view,user')->only('show');
        $this->middleware('can:update,user')->only('update');
        $this->middleware('can:delete,user')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $users = User::all();

        return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
         * unique:users indica que el email tiene que ser único en la tabla users (No puede haber otro usuario con ese email).
         * min:6 indica que el password debe trner minimo 6 caracters de longitud
         * confirmed indica que el passwor debe ser confirmado, es decir, debemos recibir un campo password_confirmation.
         */
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
        ];

        $this->validate($request, $rules);

        $fields = $request->all();
        $fields['password'] = bcrypt($request->password);
        $fields['verified'] = User::USUARIO_NO_VERIFICADO;
        $fields['verification_token'] = User::generarVerificationToken();
        $fields['admin'] = User::USUARIO_REGULAR;

        $user = User::create($fields);

        //return response()->json(['data' => $user], 201);
        return $this->showOne($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     *
     * Al hacer una inyeccion implicita del modelo, no es directamente el parametro del método, sino, es importante
     * el nombre del parametro en la ruta como tal (en singular). Para este caso, la ruta del recurso es:
     * http://localhost:8013/apirestfulpurchasesalesystem/public/api/users/1
     * el nombre del recurso es 'users', por lo tanto el nombre del parametro debe ser $user.
     */
    public function show(User $user)
    {
        //Este método disparará una excepción en caso de que la instancia del usuario solicitado no exista
        //$usuario = User::findOrFail($id);

        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        /*
         * 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR, indica que el valor enviado
         * para admin esté contemplado entre los valores User::USUARIO_ADMINISTRADOR o User::USUARIO_REGULAR.
         */
        $rules = [
            'email' => 'email|unique:users,email,' . $user->id,//Si el usuario envia su propio email,con esta regla no fallará.
            'password' => 'min:6|confirmed',
            'admin' => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR
        ];

        $this->validate($request, $rules);

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        /*
         * El email es diferente al email que actualmente tiene el usuario.
         */
        if ($request->has('email') && $user->email != $request->email) {
                $user->email = $request->email;
                $user->verified = User::USUARIO_NO_VERIFICADO;
                $user->verification_token = User::generarVerificationToken();

        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if ($request->has('admin')) {
            /*
             * Solo los usuarios administradores estan en capacidad de indicar cuando un usuario
             * es administrador o no. Es decir, si la petición incluye algo relacionado con
             * administrador vamos a verificar que ese usuario sea administrador, pues de lo
             * contrario no lo podrá hacer.
             */
            $this->allowedAdminAction();

            if (!$user->esVerificado()) {
                return $this->errorResponse('Solo los usuarios verificados pueden cambiar su valor de administrador.', 409);
            }
            $user->admin = $request->admin;
        }

        /*
         * isDirty() veriifica si alguno de los atributos originales ha cambiado respecto al valor actual.
         */
        if (!$user->isDirty()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        $user->save();

        //return response()->json(['data' => $user], 200);
        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        //return response()->json(['data' => $user], 200);
        return $this->showOne($user);
    }

    public function verify($token) {
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified = User::USUARIO_VERIFICADO;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage("La cuenta ha sido verificada.");
    }

    public function resend(User $user) {
        if ($user->esVerificado()) {
            return $this->errorResponse('Este usuario ya ha sido verificado', 409);
        }

        retry(5, function() use ($user) {
            Mail::to($user->email)->send(new UserCreated($user));
        }, 100);

        return $this->showMessage('El correo de verificación se ha reenviado.');
    }
}
