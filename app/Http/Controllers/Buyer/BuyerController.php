<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Buyer;

class BuyerController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
        * El scope 'read-general' permite hacer una visualización de casi todos los productos
        * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
        * todo lo demás.
        */
        $this->middleware('scope:read-general')->only(['show']);

        $this->middleware('can:view,buyer')->only('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        /*
         * Obtenemos solo usuarios que tengan transacciones, ya que solo los compradores tienen
         * transacciones asociadas.
         */
        /*
         * Al utilizar el BuyerScope en el modelo Buyer, ya no es necesario obtener la lista de buyers
         * de esta forma, ya que el has('transactions') se lo hace en el BuyerScope que se ejecuta de
         * manera global en cada consulta sobre Buyers.
         */
        //$buyers = Buyer::has('transactions')->get();
        $buyers = Buyer::all();

        //return response()->json(['data' => $buyers], 200);
        return $this->showAll($buyers);
    }


    /**
     * @param Buyer $buyer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Buyer $buyer)
    {
        //Obtiene solo un usuario que tenga al menos una transacción asociada y que corresponde al id de
        //usuario que se está solicitando.
        //$comprador = Buyer::has('transactions')->findOrFail($id);

        //return response()->json(['data' => $buyer],200);
        return $this->showOne($buyer);
    }
}
