<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuyerCategoryController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
        * El scope 'read-general' permite hacer una visualización de casi todos los productos
        * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
        * todo lo demás.
        */
        $this->middleware('scope:read-general')->only(['index']);

        /*
         * El middleware can recibe varios parametros, el primero es la acción que se le va a permitir 
         * (nombre de la función que se va a ejecutar en el Policy), el segundo parámetro es el recurso,
         * aqui podemos poner varias cosas, se podria pasar el nombre del modelo o inclusive una 
         * instancia de Buyer, aqui solo pasamos 'buyer' pues laravel será capaz de resolverlo, ya que 
         * la ruta que ejecuta este controlador tiene un parametro llamado buyer, asi que laravel
         * automáticamente sabrá que es esa instancia de buyer la que usaremos aqui. 
         */
        $this->middleware('can:view,buyer')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Buyer $buyer
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        /*
         * collapse() hace que una lista de listas se convierta en una lista única.
         */
        $categories = $buyer->transactions()
            ->with('product.categories')
            ->get()
            ->pluck('product.categories')
            ->collapse()
            ->unique('id')
            ->values();

        return $this->showAll($categories);
    }
}
