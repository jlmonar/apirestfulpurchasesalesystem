<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Seller;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SellerTransactionController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
         * El scope 'read-general' permite hacer una visualización de casi todos los productos
         * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
         * todo lo demás. En este caso estamos permitiendo la visualización de las transacciones
         * de un vendedor especifico.
         */
        $this->middleware('scope:read-general')->only(['index']);

        $this->middleware('can:view,seller')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Seller $seller
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $transactions = $seller->products()
            ->whereHas('transactions')
            ->with('transactions')
            ->get()
            ->pluck('transactions')
            ->collapse();

        return $this->showAll($transactions);
    }
}
