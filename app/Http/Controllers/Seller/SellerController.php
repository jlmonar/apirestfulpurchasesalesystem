<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Seller;

class SellerController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
        * El scope 'read-general' permite hacer una visualización de casi todos los productos
        * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
        * todo lo demás.
        */
        $this->middleware('scope:read-general')->only(['show']);

        $this->middleware('can:view,seller')->only(['show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        /*
         * Obtenemos solo usuarios que tengan productos, ya que solo los vendedores tienen
         * productos asociados.
         */
        //$sellers = Seller::has('products')->get();
        $sellers = Seller::all();

        return $this->showAll($sellers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Seller $seller)
    {
        //Obtiene solo un usuario que tenga al menos un producto asociado y que corresponde al id de
        //usuario que se está solicitando.
        //$seller = Seller::has('products')->findOrFail($id);

        return $this->showOne($seller);
    }


}
