<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Product;
use App\Seller;
use App\Transformers\ProductTransformer;
use App\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SellerProductController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . ProductTransformer::class)->only(['store', 'update']);

        /*
         * Se trata del scope 'manage-products', el cual permite a los clientes crear productos,
         * actualizar y eliminar un producto. Adicionalmente permite gestionar las categorias
         * de un producto, pero eso se hace en otro controlador.
         * El método index lo exceptuamos simplemente para no ser tan restrictivos en la parte de la
         * visualización de los productos.
         */
        //$this->middleware('scope:manage-products')->only(['store', 'update', 'destroy']);
        $this->middleware('scope:manage-products')->except('index');

        /*
         * El método index de este controlador, que permiten ver los productos de un vendedor específico,
         * puede ser accedido por dos diferentes scopes, manage-products y read-general, entonces, en
         * términos generales podríamos decir que solo con read-general bastaría para aplicarlo únicamente
         * al método index, y que el de manage-products se aplicaría a todos, excepto al método index.
         * Esto sería una buena aproximación y se lo podría hacer como tal:
         * $this->middleware('scope:manage-products')->only(['store', 'update', 'destroy']);
         * $this->middleware('scope:read-general')->only(['index']);
         *
         * Lo podemos hacer directamente con los middlewares de esta forma, sin embargo, a manera de ejercicio
         * y para ver que hay diferentes formas de aplicar los scopes a diferentes acciones, vamos a realizarlo
         * de la siguiente manera. En este caso, únicamente el método index puede hacer uso de
         * manage-products o read-general, el problema es que no se lo puede hacer directamente con middlewares
         * porque si ponemos que un scope y decimos que este puede utilizar manage-products y read-general:
         * $this->middleware('scope:manage-products,read-general');
         * entonces esto se va a aplicar a todas las demás acciones, quiere decir que un cliente va a poder,
         * solo con permiso de read-general eliminar, actualizar o crear productos, lo cual no tiene sentido.
         * Entonces, vamos a exceptuar de esta verificación de manage-products al método index.
         * $this->middleware('scope:manage-products')->except('index');
         * Agregamos un condicional que a partir del usuario de la petición, preguntamos si el token de ese usuario
         * tiene permisos para hacer la lectura general ('read-general') o tiene permisos para utilizar el
         * scope de 'manage-products'. En caso que esto se cunpla, se retorna la respuesta normal, caso contrario
         * entonces disparamos una nueva excepcion llamada AuthenticationException.
         */

        $this->middleware('can:view,seller')->only('index');
        $this->middleware('can:sale,seller')->only('store');
        /*
         * Aqui cambia el nombre del valor del primer parametro que enviamos, ya que el nombre del método
         * en SellerPolicy es editProduct y en el middleware es edit-product. Laravel se encarga automáticamente
         * de esta conversión, pero para ello es importante definirlo en el middleware separado por '-'.
         */
        $this->middleware('can:edit-product,seller')->only('update');
        $this->middleware('can:delete-product,seller')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Seller $seller
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthenticationException
     */
    public function index(Seller $seller)
    {
        /*
         * Agregamos un condicional que a partir del usuario de la petición, preguntamos si el token de ese usuario
         * tiene permisos para hacer la lectura general ('read-general') o tiene permisos para utilizar el
         * scope de 'manage-products'. En caso que esto se cumpla, se retorna la respuesta normal, caso contrario
         * entonces disparamos una nueva excepcion llamada AuthenticationException.
         */
        if (request()->user()->tokenCan('read-general') || request()->user()->tokenCan('manage-products')) {
            $products = $seller->products;

            return $this->showAll($products);
        }

        throw  new AuthenticationException;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Seller $seller
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Seller $seller)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required',
            'quantity' => 'required|integer|min:1',
            'image' => 'required|image',
        ];

        $this->validate($request, $rules);

        $data = $request->all();

        $data['status'] = Product::PRODUCTO_NO_DISPONIBLE;
        $data['image'] = $request->image->store('');
        $data['seller_id'] = $seller->id;

        $product = Product::create($data);

        return $this->showOne($product, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @param  \App\Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller, Product $product)
    {
        $rules = [
            'quantity' => 'integer|min:1',
            'status' => 'in' . Product::PRODUCTO_DISPONIBLE . ',' . Product::PRODUCTO_NO_DISPONIBLE,
            'image' => 'image',
        ];

        $this->validate($request, $rules);

        //Verificar porque no funciona con la funcion, salta error cuando se usa un vendedor no real del producto
        //$this->verificarVendedor($seller, $product);
        if ($seller->id != $product->seller_id) {
            return $this->errorResponse('El vendedor especificado no es el vendedor real del producto', 422);
        }


        $product->fill($request->intersect([
            'name',
            'description',
            'quantity'
        ]));

        if ($request->has('status')) {
            $product->status = $request->status;

            if ($product->estaDisponible() && $product->categories()->count() == 0) {
                return $this->errorResponse('Un producto activo debe tener al menos una categoría', 409);
            }
        }

        if ($request->hasFile('image')) {
            Storage::delete($product->image);

            $product->image = $request->image->store('');
        }

        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        $product->save();

        return $this->showOne($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @param  \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product)
    {
        //Verificar porque no funciona con la funcion, salta error cuando se usa un vendedor no real del producto
        //$this->verificarVendedor($seller, $product);
        if ($seller->id != $product->seller_id) {
            return $this->errorResponse('El vendedor especificado no es el vendedor real del producto', 422);
        }

        Storage::delete($product->image);

        $product->delete();

        return $this->showOne($product);
    }

    protected function verificarVendedor(Seller $seller, Product $product) {
        if ($seller->id != $product->seller_id) {
            throw new HttpException(422, 'El vendedor especificado no es el vendedor real del producto');
        }
    }
}
