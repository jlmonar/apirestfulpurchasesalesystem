<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Seller;
class SellerCategoryController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
        * El scope 'read-general' permite hacer una visualización de casi todos los productos
        * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
        * todo lo demás.
        */
        $this->middleware('scope:read-general')->only(['index']);

        /*
         * En esta ocasión usamos el middleware 'can', donde enviamos como primer parámetro el
         * valor 'view' (que corresponde a la función 'view' definida en SellerPolicy) y como
         * segundo parametro el nombre del modelo 'seller'.
         * Aplicamos el middleware únicamente al método index.
         */
        $this->middleware('can:view,seller')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Seller $seller
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $categories = $seller->products()
            ->with('categories')
            ->get()
            ->pluck('categories')
            ->collapse()
            ->unique('id')
            ->values();

        return $this->showAll($categories);
    }
}
