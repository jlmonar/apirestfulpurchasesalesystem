<?php

namespace App\Http\Controllers\Product;

use App\Buyer;
use App\Http\Controllers\ApiController;
use App\Product;
use App\Transaction;
use App\Transformers\TransactionTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . TransactionTransformer::class)->only(['store']);
        /*
         * Se trata del scope que permite comprar productos, entonces regisramos en este controlador
         * el middleware, ya que es aquí donde se crean las transacciones. En este middleware enviamos
         * como parametro los nombres de los scopes que queremos validar, en este caso solamente
         * 'purchase-product', pero si quisieramos la validación para varios scopes, entonces podriamos
         * pasarlos separandolos por comas. Adicionalmente, necesitamos que este scope particular sea
         * validado únicamente para el método store.
         */
        $this->middleware('scope:purchase-product')->only(['store']);

        /*
         * Em esta ocasión usamos el middleware 'can', donde esta vez enviamos como primer parámetro el
         * valor 'purchase' (que corresponde a la función purchase definida en BuyerPolicy).
         * Aplicamos el middloeware únicamente al método store, que es donde se realiza una compra.
         */
        $this->middleware('can:purchase,buyer')->only('store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Product $product
     * @param Buyer $buyer
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, Buyer $buyer)
    {
        $rules = [
            'quantity' => 'required:min:1'
        ];

        $this->validate($request, $rules);

        if ($buyer->id == $product->seller_id) {
            return $this->errorResponse('El comprador debe ser diferente al vendedor', 409);
        }

        if (!$buyer->esVerificado()) {
            return $this->errorResponse('El comprador debe ser un usuario verificado', 409);
        }

        if (!$product->seller->esVerificado()) {
            return $this->errorResponse('El vendedor debe ser un usuario verificado', 409);
        }

        if (!$product->estaDisponible()) {
            return $this->errorResponse('El producto para esta transacción no está disponible', 409);
        }

        if ($product->quantity < $request->quantity) {
            return $this->errorResponse('El producto no tiene la cantidad disponible requerida para esta transacción', 409);
        }

        return DB::transaction(function () use ($request, $product, $buyer) {
            $product->quantity -= $request->quantity;
            $product->save();

            $transaction = Transaction::create([
                'quantity' => $request->quantity,
                'buyer_id' => $buyer->id,
                'product_id' => $product->id,
            ]);

            return $this->showOne($transaction, 201);
        });
    }
}
