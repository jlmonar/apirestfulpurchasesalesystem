<?php

namespace App\Http\Controllers\Product;

use App\Category;
use App\Http\Controllers\ApiController;
use App\Product;
use Illuminate\Http\Request;

class ProductCategoryController extends ApiController
{
    public function __construct()
    {
        //Se aplica el middleware para todos los métodos, excepto para index.
        $this->middleware('auth:api')->except(['index']);
        $this->middleware('client.credentials')->only(['index']);

        /*
         * Se trata del scope 'manage-products', el cual permite a los clientes crear productos,
         * actualizar y eliminar un producto. Adicionalmente permite gestionar las categorias
         * de un producto, esto es, agregar o eliminar categorias a un producto.
         * El método index lo exceptuamos ya que esto es información pública (definido por el
         * client-credentials).
         */
        $this->middleware('scope:manage-products')->except(['index']);

        /*
         * El middleware can recibe como parámetro el método add-category y el producto, este
         * middleware restringe que una categoria puede ser agregada a un producto solo por el
         * vendedor real de ese producto especifico.
         */
        $this->middleware('can:add-category,product')->only('update');
        /*
         * Este middleware restringe que una categoria puede ser DESLIGADA DE un producto solo
         * por el vendedor real de ese producto especifico.
         */
        $this->middleware('can:delete-category,product')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $categories = $product->categories;

        return $this->showAll($categories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  \App\Product  $product
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Category $category)
    {
        //sync, attach,sincWithoutDetaching

        //SYNC: sustituye TODA la lista de categorias por la nueva categoria.
        //$product->categories()->sync([$category->id]);

        //ATTACH: no diferencia en la existencia de la catoria asociada al producto, de modo que
        //si se hace una petición tratando de asociar la misma categoria al misma producto varias
        // veces existirán varios registros de la misma categoráa para el producto
        //$product->categories()->attach([$category->id]);

        //SYNCWITHOUTDETACHING: Agregará la nueva categoría sin eliminar las que ya existían.
        $product->categories()->syncWithoutDetaching([$category->id]);

        return $this->showAll($product->categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Category $category)
    {
        if (!$product->categories()->find($category->id)) {
            return $this->errorResponse('La categoría especificada no es una categoría de este producto', 404);
        }

        $product->categories()->detach([$category->id]);

        return $this->showAll($product->categories);
    }
}
