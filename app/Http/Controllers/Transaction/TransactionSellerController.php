<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\ApiController;
use App\Transaction;

class TransactionSellerController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
         * El scope 'read-general' permite hacer una visualización de casi todos los productos
         * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
         * todo lo demás. En este caso estamos permitiendo la visualización del vendedor de
         * una transacción en específico.
         */
        $this->middleware('scope:read-general')->only(['index']);

        /*
         * El middleware can recibe como parámetro el método view y la transacción, este middleware
         * permite visualizar el vendedor de una transacción especifica unicamente al comprador
         * o vendedor involucrado en la transacción.
         */
        $this->middleware('can:view,transaction')->only('index');

    }

    /**
     * Display a listing of the resource.
     *
     * @param Transaction $transaction
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        $seller = $transaction->product->seller;

        return $this->showOne($seller);
    }
}
