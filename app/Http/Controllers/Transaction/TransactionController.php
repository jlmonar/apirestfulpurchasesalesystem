<?php

namespace App\Http\Controllers\Transaction;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class TransactionController extends ApiController
{
    public function __construct()
    {
        /*
         * Llamamos al constructor de la clase padre donde definimos que se usará
         * el middleware 'auth:api'.
         */
        parent::__construct();

        /*
         * El scope 'read-general' permite hacer una visualización de casi todos los productos
         * disponibles que tenemos, categorias, ventas, compras o transacciones en general y
         * todo lo demás. En este caso lo aplicamos solamente en el método show, el cual permite
         * visualizar las transacciones de ese usuario especifico, porque a fin de cuentas es el 
         * usuario quien le está dando permiso al cliente por medio del acces_token de realizar 
         * las operaciones. (??)
         */
        $this->middleware('scope:read-general')->only(['show']);

        /*
         * El middleware can recibe como parámetro el método view y la transacción, este middleware
         * restringe que una transacción específica se pueda ver solo po el comprador y el vendedor de
         * esa transacción.
         */
        $this->middleware('can:view,transaction')->only('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $transactions = Transaction::all();

        return $this->showAll($transactions);
    }

    /**
     * Display the specified resource.
     *
     * @param  Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        return $this->showOne($transaction);
    }
}
