<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ApiController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        /*
         * Usamos el middleware 'auth' con el guard 'api'.
         */
        $this->middleware('auth:api');
    }
    
    protected function allowedAdminAction() {
        if (Gate::denies('admin-action')) {
            throw new AuthorizationException('Esta acción no la tienes permitida.');
        }
    }
}
