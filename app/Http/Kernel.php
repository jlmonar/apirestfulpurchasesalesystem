<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

/**
 * En esta clase se configura la manera en la que se llamará o se utilizarán
 * los middlewares en nuestro sistema.
 */
class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            'signature:X-Application-Name',
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            /*
             * Vamos a incluir las cabeceras de CORS unicamente en las rutas relacionadas con
             * la API. Registramos el middleware bajo un nombre en $routeMiddleware.
             * Es bueno colocar 'cors' al comienzo, para que si alguno de los otros middlewares
             * fallan, pues no tenga problemas porque 'cors' ya se ha ejecutado.
             */
            'cors',
            'signature:X-Application-Name',
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,

        /*
         * Middleware que nos permite proteger algunas rutas que pueden ser de accceso general, para
         * proteger estas rutas, puesto que no queremos que cualquier persona sin ningún tipo de
         * validación este accediendo, vamos a protegerlas con algo que se conoce como 'Client grant type',
         * este es una manera de proporcionar acceso a un cliente valido de nuestro sistema sin necesidad
         * de que sea autorizado por un usuario. De este modo, cualquier cliente registrado en nuestro
         * sistema, va a poder acceder a esas rutas sin necesidad de tener autorización de un usuario.
         * Esto es para proteger las rutas más basicas de nuestro sistema.
         */
        'client.credentials' => \Laravel\Passport\Http\Middleware\CheckClientCredentials::class,

        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \App\Http\Middleware\CustomThrottleRequests::class,

        /*
         * Middleware que verifica que el acces token ercibido esté haciendo uso de la menos uno
         * de los scopes definidos.
         */
        'scope' => \Laravel\Passport\Http\Middleware\CheckForAnyScope::class,
        /*
         * Middleware que verifica que todos los scopes hayan sigo garantizados para ese acces token.
         */
        'scopes' => \Laravel\Passport\Http\Middleware\CheckScopes::class,

        'signature' => \App\Http\Middleware\SignatureMiddleware::class,
        'transform.input' => \App\Http\Middleware\TransformInput::class,

        'cors' => \Barryvdh\Cors\HandleCors::class,
    ];
}
