<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponser;
use Closure;
use Illuminate\Routing\Middleware\ThrottleRequests;

/**
 * Los middlewares son elementes que se ejecutan antes de atender una petición dada,
 * buscando realizar modificaciones o comprobaciones en la misma. Por ejemplo, se
 * puede crear un middleware que verifique que el usuario que está realizando la
 * petición, sea un administrador, o se tiene otro middleware que verifique que
 * el usuario que está realizando la petición a un recurso en específico, haya
 * iniciado sesión.
 */
class CustomThrottleRequests extends ThrottleRequests
{
    use ApiResponser;
    /**
     * Create a 'too many attempts' response.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function buildResponse($key, $maxAttempts)
    {
        //$response = new Response('Too Many Attempts.', 429);
        $response = $this->errorResponse('Too Many Attemps', 429);

        $retryAfter = $this->limiter->availableIn($key);

        return $this->addHeaders(
            $response, $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );
    }
}
