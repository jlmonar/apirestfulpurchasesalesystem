<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;

/*
 * Este middleware es creado con el fin de resolver el problema que se da con los parametros enviados
 * por el usuario al momento de usar los métodos POST, PUT ó PATCH (para insertar o actualizar registros)
 * ya que estos parametros no están siendo transformados segun los valores que tenemos en los Transformers,
 * sino que estos estan siendo enviados por el usuario tal y como tenemos los campos en nuestra base de datos
 * lo cual es un problema de integridad, ya que estamos dando a conocer la estructura de nuestra base
 * de datos. Este middleware nos va a permitir enviar directamente los campos transformados y que estos
 * sean aceptados realmente, aunque de manera independiente, nuestra api restful esté transformando
 * dichos campos en el valor real de los atributos originales que correspondan.
 */
class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $transformer)
    {
        $transformedInput = [];

        //$request->request->all(): obtenemos los campos recibidos unicamente en el cuerpo de la
                                 // consulta y no el la url.
        //Vamos a recorrer cada uno de los campos recibidos  unicamente en el cuerpo de la consulta y no
        //los parametros en la url, esto lo hacemos a través de la petición y accediendo a un campo llamado
        //request que es quien tiene lo relacionado con la petición y no con la url o parametros de consulta.
        foreach ($request->request->all() as $input => $value) {
            //Este nuevo array en su posición del valor real del atributo a partir de lo que recibimos sea
            //igual a ese valor que recibimos originalmente, para ello utilizamos el transformer::originalAttribute
            $transformedInput[$transformer::originalAttribute($input)] = $value;
        }

        $request->replace($transformedInput);

        /*
         * Un segundo problema que hay es con las respuestas que se dan en las peticiones cuando estas tienen
         * errores por validaciones, ya sea que se envio mal un parametro o falto enviar alguno. En estos
         * casos el mensaje de error muestra el nombre del campo que tiene el error, pero el muestra el nombre
         * del campo tal y como lo tenemos en nuestra base de datos, esto no debe ser asi ya que debe mostrar
         * el nombre de los campos transformados, para resolver esto ahora trabajamos sobre la respuesta y
         * hacemos uso de la funcion transformedAttributte que definimos en cada una de las transformaciones.
         */
        /*
         * Hacemos una modificación ya no sobre la petición, ni los campos de la petición, sino sobre la respuesta,
         * especificamente, los campos de error de la respuesta. Hay muchos tipos de errores, por lo tanto debemos
         * asegurarnos de estar atendiendo únicamente a respuestas que tengan errores y que ese error sea una
         * instancia de ValidationException, asi sabremos que podemos modificar los datos al interior de esta
         * respuesta sustituyendo los valores de los atributos originales por los que corresponden a los de la
         * transformación.
         */
        $response = $next($request);

        if (isset($response->exception) && $response->exception instanceof ValidationException) {
            /*
             * Al interior de los datos de la excepción, tenemos una serie de errores, la cual contiene un atributo
             * 'error' que es el que nos interesa, el cual contiene un array con cada uno de los campos que falló
             * en la validación, por lo tanto lo que se hará es sustituir los valores tanto de las claves de ese
             * array como los valores que aparezcan dentro de la descripción de cada error.
             */
            $data = $response->getData();

            $transformedErrors = [];

            foreach ($data->error as $field => $error) {
                //Obtenemos el valor del campo transformado a partir del original.
                $transformedField = $transformer::transformedAttribute($field);
                //Construimos la nueva lista de errores pero utilizando los nuevos nombres
                //str_replace: lo usamos para sustituir todas las posibles ocurrencias que hayan del valor anterior
                //del campo (del atributo original), por el atributo transformado.
                $transformedErrors[$transformedField] = str_replace($field, $transformedField, $error);
            }

            //Asignamos la lista de errores transformada.
            $data->error = $transformedErrors;

            $response->setData($data);
        }
        return $response;
    }
}
