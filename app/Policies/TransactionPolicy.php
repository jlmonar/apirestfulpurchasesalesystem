<?php

namespace App\Policies;

use App\Traits\AdminAction;
use App\User;
use App\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization, AdminAction;

    /**
     * Determine whether the user can view the transaction.
     *
     * @param  \App\User  $user
     * @param  \App\transaction  $transaction
     * @return mixed
     */
    public function view(User $user, Transaction $transaction)
    {
        /*
         * Dos usuarios posibles pueden visualizar una transacción, el vendedor de la transacción
         * y el comprador de esa transacción, ambos van a tener permisos para  visualizar los
         * detalles de la transacción específica o inclusive ver el vendedor, es decir, el comprador
         * puede ver quien fue el vendedor que el vendión el producto.
         */
        return $user->id === $transaction->buyer->id || $user->id === $transaction->product->seller->id;
    }
}
