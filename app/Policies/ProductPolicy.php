<?php

namespace App\Policies;

use App\Traits\AdminAction;
use App\User;
use App\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization, AdminAction;

    /**
     * Determina cuando un usuario puede agregar una nueva categoría a un producto.
     *
     * @param  \App\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function addCategory(User $user, Product $product)
    {
        /*
         * Tenemos que validar que el usuario que está realizando esta operación, sea realmente
         * el vendedor de ese producto, no el comprador, debe ser EL VENDEDOR. Esto es porque vamos
         * a restringir dos posibles acciones, la posibilidad de agregar una categoria o de eliminar
         * una categoría del producto, no del sistema, solamente desligarla del producto, y eso
         * deberia poder hacerlo solo el vendedor y no el comprador.
         */
        return $user->id === $product->seller->id;
    }

    /**
     * Determina cuando un usuario puede desligar una categría de un producto.
     *
     * @param  \App\User  $user
     * @param  \App\Product  $product
     * @return mixed
     */
    public function deleteCategory(User $user, Product $product)
    {
        return $user->id === $product->seller->id;
    }
}
