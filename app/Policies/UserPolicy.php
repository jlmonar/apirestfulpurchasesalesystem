<?php

namespace App\Policies;

use App\Traits\AdminAction;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization, AdminAction;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $authenticatedUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->id === $user->id;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $authenticatedUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $authenticatedUser, User $user)
    {
        return $authenticatedUser->id === $user->id;
    }

    /**
     * Determine whether the user can delete the user.
     * Permitimos únicamente el usuario real eliminar su cuenta.
     *
     * @param  \App\User  $authenticatedUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $authenticatedUser, User $user)
    {
        /*
         * Tenemos que verificar que el usuario autenticado es el usuario real y propietario de la
         * cuenta, además, tenemos que vericicar que el acces_token que se está usando realmente
         * pertenezca únicamente a ese usuario y no sea un cliente que está haciendo la petición.
         * Podríamos hacer esto a través de la interfaz web, agregando un botón para eliminar la cuenta
         * del usuario logueado, de ese modo sabemos que el usuario logueado es el usuario real.
         * Sin embargo, queremos mantener tod.o en la API en la medida de lo posible.
         * Recordar que existe un tipo de acces_token que únicamente se podría obtener desde la interfaz
         * de usuario, el cual es el token personal, entonces lo que haremos es verificar que el token
         * que está enviando el usuario autenticado sea un token que pertenece a un cliente de tipo personal.
         */
        /*
         * personal_access_client: es verdadero cuando ese cliente puede crear tokens personales.
         */
        return $authenticatedUser->id === $user->id && $authenticatedUser->token()->client->personal_access_client;
    }
}
