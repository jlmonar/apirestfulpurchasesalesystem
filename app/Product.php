<?php

namespace App;

use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    const PRODUCTO_DISPONIBLE = 'disponible';
    const PRODUCTO_NO_DISPONIBLE = 'no disponible';

    public $transformer = ProductTransformer::Class;

    protected  $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id',
    ];

    protected $hidden = [
        'pivot'
    ];

    public function estaDisponible() {
        return $this->status == Product::PRODUCTO_DISPONIBLE;
    }

    /**
     * El producto PERTENECE a UN vendedor (esto debido a que es producto quien lleva
     * la clave foránea 'seller_id')
     */
    public function seller() {
        return $this->belongsTo(Seller::class);
    }

    /**
     * Un producto TIENE muchas transacciones.
     */
    public function transactions() {
        return $this->hasMany(Transaction::class);
    }

    /**
     * El producto PERTENECE a muchas categorias (Aqui estamos lidiando con una relación muchos a muchos
     * entre categories y products)
     */
    public function categories() {
        return $this->belongsToMany(Category::class);
    }
}
