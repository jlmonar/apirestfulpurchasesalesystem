
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));
/*
 * Registro componente relacionado con la gestión de tokens personales.
 */
Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

/*
 * Registro componente que me permite visualizar los clientes que son propiedad del
 * usuario que está autenticado en ese momento.
 */
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

/*
 * Registro componente que me permite gestionar que los clientes que un usuario autenticado
 * ha autorizado, sean de su propiedad o no.
 */
Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

const app = new Vue({
    el: '#app'
});
