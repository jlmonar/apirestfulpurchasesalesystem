<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel CORS
    |--------------------------------------------------------------------------
    |
    | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
    | to accept any value.
    |
    | Usualmente la configuración por defecto es adecuada, porque básicamente que
    | queremos es permitir el acceso para todos los origenes, es decir, cualquier
    | ubicación desde la que nos envian la petición, para cualquier cantidad de cabeceras
    | para cualquier método HTTP, etc.
    | Sin embargo, se podrían necesitar ciertas restricciones, por ejemplo:
    | Permitir únicamente el métido GET,  el método GET con el método HEAD
    | 'allowedMethods' => ['GET', 'HEAD'],
    | hay que aclarar, que si permitimos el método POST, entonces estariamos permitiendo
    | todos los demás métodos, ya que por medio del método POST, nos van a poder indicar en laravel
    | que están haciendo en realidad una petición PUT o PATCH indicando el --method, por lo
    | tando, permitir POST sería casi que lo mismo que permitir todos los demás métodos.
    | También podríamos especificar cualquier dominio o URL de la cual queramos permitir
    | únicamente ese acceso.
    | 'allowedOrigins' => ['example.com'],
    |
    */
   
    'supportsCredentials' => false,
    'allowedOrigins' => ['*'],
    'allowedOriginsPatterns' => [],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['*'],
    'exposedHeaders' => [],
    'maxAge' => 0,

];
